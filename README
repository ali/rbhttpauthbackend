.. _reviewboard: http://www.reviewboard.org/
.. _reviewboard legacy backend: http://www.reviewboard.org/docs/codebase/dev/extending/legacy-auth-backends/


Introduction
============
RBHTTPAuthBackend implements a `reviewboard legacy backend`_ to be used with `reviewboard`_ - if you have protected your installation with basic http authentication. 
It allows your users to authenticate with reviewboard using the same credentials as the http authentication they use to access the reviewboard site.


Setup
=====
#. Install RBHTTPAuthBackend::

    # using pip
    sudo pip install RBHTTPAuthBackend

    # using easy_install
    sudo easy_install RBHTTPAuthBackend

    # from source
    git clone http://hg.indydevs.org/rbhttpauthbackend && cd rbhttpauthbackend && sudo python setup.py build install
#. Restart your web server running review board
#. Login in to review board as administrator
    #. Go to Admin panel
    #. Settings > Authentication
    #. In the 'Authentication method' drop down, select 'Legacy Authentication Module'
    #. Enter 'RBHTTPAuthBackend.RBHTTPAuthBackend' in the 'Backends' edit box.
    #. Click on 'Save'


